<?php
if (!empty($this->data['htmlinject']['htmlContentPost'])) {
    foreach ($this->data['htmlinject']['htmlContentPost'] as $c) {
        echo $c;
    }
}
?>
            </div><!-- #content -->
            <div id="footer">
                <hr />
<?php
$config = \SimpleSAML\Configuration::getInstance();
$footerText = $config->getString('theme.footer', null);
if(! $footerText) {
?>
                <img src="/<?php echo $this->data['baseurlpath']; ?>resources/icons/ssplogo-fish-small.png" alt="Small fish logo" style="float: right" />
                    Copyright &copy; 2007-2019 <a href="http://uninett.no/">UNINETT AS</a>

                <br style="clear: right" />

<?php
} else {
    echo $footerText;
}
?>
            </div><!-- #footer -->
        </div><!-- #wrap -->
    </body>
</html>
