/**
 * Language selector
 */
function langSelectorChange(el) {
    var optionValue = el.options[el.selectedIndex].value;

    window.location = optionValue;
}
